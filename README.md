# JPF-Android Development Environment Docker Image

This container is a development environment used to build Android apps (targets 15/19) and JPF projects and to run Android apps using JPF-Android.

This repository contains the Dockerfile and folders to build an image containing:

- Official Ubuntu 14.04
- Official Oracle Java 1.8.60-b27
- Ant 1.9.8
- JUnit4
- Android
    - tools
    - build-tools
    - platform-tools
    - targets 10 15 and 19 (without emulator)
- python2.7 + pip (for logparser)
- vim
- git
- mercurial
- mongo 3.0
    + mongo client
    + mongo tools
    
It expects:

- Volume /JPF - containing JPF projects
- Volume /APP - containing  apps and jpf-android-examples
- Volume /RESULTS - stores the results
- mongodb docker to be linked (optional if used)

If the JPF projects, apps and mongodb content has not been setup the user can run
`./setup.sh` in the docker's terminal to download and build the projects and insert the mongodb content.

## Docker Tags

0.4.3

## Installation

To build and run this docker image clone this repo and run:

    docker build -t jpa:0.4.3 .

The built image is 1.906 GB in size.

## Usage

This docker runs as an interactive terminal giving the user a terminal back when docker run is called. `--rm` removes the docker container and its file system after the terminal session is closed. 

### Start the container

    mkdir docker
    cd docker
    
    # start mongodb with data persisted in data/db    
    mkdir -p data/db
    docker run -v data/db:/data/db -p 27017:27017 --name mongodb3 -d mongo:3.0 
    
    # setup folder structure
    mkdir APPS
    mkdir JPF
    mkdir RESULTS
    
    # pull and run a jpa container
    docker run -v JPF:/JPF -v APPS:/APPS -v RESULTS:/RESULTS --link mongodb3:mongo --rm -it  jpa:0.4.3
    
    # setup code base / pull latest changes and build workspace
    root@652f10eb3ae4: chmod +x setup.sh
    root@652f10eb3ae4: ./setup.sh
    root@652f10eb3ae4: mongoimport --host=mongodb3 --port=27017 --db events_db --collection events --file events_exported.json 
    
    # run jpf-android-examples
    cd jpf-android-examples
    ./run.sh Calculator +event.strategy=default


## NOTE

- docker  can not run together with the Android emulator in OSX
- all files written to volumes are persisted (JPF, APPS and RESULTS)
- the `/storage` directory models the storage on Android devices but since xml parsers are broken, we needed to manually fix paths in the models. 
- more Android targets can be added to image using 
 
        android list sdk 
        android update sdk --no-ui -a --filter 2,3,4,...

    but will not be persisted if not added to the Dockerfile.

- due to a bug in docker for OSX the time of the docker is not set correctly when the docker goes to sleep. You need to restart the docker. (see https://github.com/docker/for-mac/issues/17)