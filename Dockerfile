FROM ubuntu:14.04
MAINTAINER Heila Botha <heilamvdm@gmail.com>

# Setup Ubuntu
RUN apt-get -qq update && \
    apt-get -qq install -y ant \
                       curl \
                       unzip \
                       junit4 \
                       git \
                       mercurial \
                       vim  && \
    apt-get -qq autoclean && apt-get --purge -y autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Setup junit4
ENV JUNIT_HOME /usr/share/java
ENV CLASSPATH $CLASSPATH:$JUNIT_HOME/junit-4.11.jar:$JUNIT_HOME/hamcrest-core-1.3.jar

# add webupd8 repository
RUN \
    echo "===> add webupd8 repository..."  && \
    echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list  && \
    echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list  && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886  && \
    apt-get update  && \
    \
    \
    echo "===> install Java"  && \
    echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections  && \
    echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections  && \
    DEBIAN_FRONTEND=noninteractive  apt-get install -y --force-yes oracle-java8-installer oracle-java8-set-default  && \
    \
    \
    echo "===> clean up..."  && \
    rm -rf /var/cache/oracle-jdk8-installer  && \
    apt-get clean  && \
    rm -rf /var/lib/apt/lists/*

# Install Android sdk
WORKDIR /sdk 
RUN curl --silent --retry 3 https://dl.google.com/android/repository/tools_r25.2.3-linux.zip -o tools_r25.2.3-linux.zip && \
    unzip tools_r25.2.3-linux.zip && \
    rm tools_r25.2.3-linux.zip

ENV PATH $PATH:/sdk/tools:/sdk/build-tools:/sdk/platform-tools
ENV ANDROID_HOME /sdk
ENV ANDROID_SDK /sdk

RUN android list sdk --all && \
    echo "y" | android update sdk --no-ui --all --filter 2,3,48,52,57

# Mount JPF, Apps and Results
RUN mkdir /JPF
RUN mkdir /RESULTS
RUN mkdir /APPS
VOLUME /JPF
VOLUME /APPS
VOLUME /RESULTS

ENV JPF /JPF

# Setup site.properties
RUN mkdir -p /root/.jpf
COPY site.properties /root/.jpf

# Setup storage
COPY storage /storage

WORKDIR /

# Python, pip and libs
#RUN apt-get update && apt-get install -y python-pip && \ 
#    pip install bson && \
#    pip install pymongo && \
#    apt-get autoclean && apt-get --purge -y autoremove && \
#    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Mongo-client
# RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 && \
#     echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.0.list && \
#     apt-get -qq update && \
#     apt-get -qq install mongodb-org-shell=3.0.13 mongodb-org-tools=3.0.13 && \
#     apt-get -qq autoclean && apt-get --purge -y autoremove && \
#     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


COPY setup.sh /
RUN chmod +x setup.sh
# COPY logparser.py /
# COPY newdbexport.json /
ENV force_color_prompt yes

ENV TZ=Africa/Johannesburg 
RUN echo $TZ | tee /etc/timezone 
RUN dpkg-reconfigure --frontend noninteractive tzdata
