#!/bin/bash

cd /JPF

# Get JPF
if [ ! -e "jpf-core" ]; then
	echo "Cloning jpf-core ..."
    hg clone http://babelfish.arc.nasa.gov/hg/jpf/jpf-core -r 820b89dd6c97
    mkdir -p jpf-core/lib
fi
cd jpf-core
echo "Building jpf-core ..."	
ERR=`ant clean build dist | tail -n2`
echo $ERR
cd /JPF

# Get nHandler
if [ ! -e "jpf-nhandler" ]; then
	echo "Cloning jpf-nhandler ..."
    hg clone http://bitbucket.org/nastaran/jpf-nhandler
fi
cd jpf-nhandler
echo "Building nhandler ..."	
ERR=`ant clean build dist | tail -n2`
echo $ERR
cd /JPF

# Get JPF-Android
if [ ! -e "jpf-android" ]; then
	echo "Cloning jpf-android ..."	
    git clone https://bitbucket.org/heila/jpf-android
else
	echo "Pull changes from jpf-android ..."	
    cd jpf-android
    git pull
fi
cd /JPF/jpf-android/jpf-android
echo "Building jpf-android ..."	
ERR=`ant clean build dist | tail -n2`
echo $ERR

echo "Copying changes from jpf-android to jpf-core"	

# make jpf-core corrections
cd jpf-core-changes

cp AllRunnablesSyncPolicy.java  /JPF/jpf-core/src/main/gov/nasa/jpf/vm
cp ClassLoaderInfo.java         /JPF/jpf-core/src/main/gov/nasa/jpf/vm
cp GenericSharednessPolicy.java /JPF/jpf-core/src/main/gov/nasa/jpf/vm
cp NativePeer.java              /JPF/jpf-core/src/main/gov/nasa/jpf/vm
cp OVStatics.java               /JPF/jpf-core/src/main/gov/nasa/jpf/vm
cp FilteringSerializer.java     /JPF/jpf-core/src/main/gov/nasa/jpf/vm/serialize

cp JPF_java_lang_ThreadLocal.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm
cp JPF_java_net_URLEncoder.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm
cp JPF_java_security_MessageDigest.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm
cp JPF_java_util_regex_Matcher.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm
cp JPF_java_io_File.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm
cp JPF_java_io_FileDescriptor.java /JPF/jpf-core/src/peers/gov/nasa/jpf/vm


cp Format.java /JPF/jpf-core/src/classes/java/text
cp Matcher.java /JPF/jpf-core/src/classes/java/util/regex
cp MessageDigest.java /JPF/jpf-core/src/classes/java/security
cp NumberFormat.java /JPF/jpf-core/src/classes/java/text
cp RandomAccessFile.java /JPF/jpf-core/src/classes/java/io
cp FileInputStream.java /JPF/jpf-core/src/classes/java/io

cp Config.java /JPF/jpf-core/src/main/gov/nasa/jpf

cd /JPF/jpf-core
echo "Building jpf-core again ..."	
ERR=`ant clean build dist | tail -n2`
echo $ERR

# make APPS
cd /APPS

# get jpf examples 
if [ ! -e "jpf-android-examples" ]; then
	echo "Cloning jpf-android-examples ..."
	git clone https://bitbucket.org/heila/jpf-android-examples
else 
	echo "Pull changes from jpf-android-examples ..."
	cd jpf-android-examples
	git pull
fi
cd /APPS/jpf-android-examples
echo "Building jpf-android-examples ..."	
ERR=`ant clean build dist | tail -n2`
echo $ERR

#get & build apps
cd /APPS
if [ ! -e "build.xml" ]; then
	echo "Downloading apps ..."
    curl   --silent --location --retry 3 "https://bitbucket.org/heila/jpf-android-examples/downloads/APPS.tar.gz" -o "APPS.tar.gz"
	echo "Extracting apps ..."
    tar xfz APPS.tar.gz
    echo "Removing tmp files ..."
    find . -name '.DS_Store' -type f -delete
    find . -name '._*' -type f -delete
fi
echo "Building Android projects ..."
ERR=`ant clean`
ERR=`ant build` | tail -n 2
echo $ERR

# put values in mongodb
#mongoimport --host=mongodb3 --port=27017 --db events_db --collection events --file newdbexport.json 
