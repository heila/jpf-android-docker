#!/usr/local/bin/python
import bson
import pymongo
from datetime import datetime
import json
import sys
import re

app_name = "Ringdroid"
app_package = "com.ringdroid"
run_number = 1

def parseEvent(line):
    event = {}

    class_info = re.findall(r'(.+)params=\{', line)[0]
    info = class_info.split(' ') 
    l = len(info)
    if (l  >= 5): 
        android_version = info[l-5]
        classname = info[l-4]
        method_name = info[l-3]
        param_signature = info[l-2]
        
        event['classname'] = classname
        event['method_signature'] = method_name + param_signature 
        event['android_version'] = android_version
        event['params'] = re.findall(r'params=\{([^}]+)\}', line)[0]
        event['ret'] =  re.findall(r'ret=\{([^}]+)\}', line)[0]
        event['app_name'] = app_name
        event['app_package'] = app_package
        event['run_number'] = run_number
        uri =  re.findall(r'uri=\{([^}]+)\}', line)
        if len(uri) > 0 :
            event['uri'] = uri[0]

        print event
    return event






def main(file):

    conn = pymongo.MongoClient()
    db_name = app_name.lower() + '_db'
    db = conn[db_name]
    events = []

    for line in file:
        try:
            e = parseEvent(line)
            if not e in events:
                events.append(e)
                # this only inserts if these fields differ
                #db.events.update({'classname': e['classname'], 'method_signature' : e['method_signature'], 'params' : e['params'], 'ret' : e['ret']}, e, upsert=True)        
        except Exception, err:
            print err
            return 1

    return 0



if __name__ == '__main__':

    if len(sys.argv) == 3:
        filename = sys.argv[1]
        app_name = sys.argv[2]
        package_name = sys.argv[3]

        f = file(filename)
        sys.exit(main(f))
    else:
        print "Usage: ./logparser.py filename app_name package_name" 

  
